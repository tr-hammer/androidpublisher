import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.androidpublisher.AndroidPublisher
import com.google.api.services.androidpublisher.AndroidPublisherScopes
import com.google.api.services.androidpublisher.model.Track
import com.google.api.services.androidpublisher.model.TrackRelease
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.GoogleCredentials
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.gson.GsonBuilder
import java.io.FileInputStream

private const val serviceAccountKey = "truckers_report_service_account.json"
private const val packageName = "com.truckersreport.hammer"
private val gson = GsonBuilder().setPrettyPrinting().create()

fun main() {
    val credentials = googleCredentials()
    val service = androidPublisherService(credentials)
    val edits = service.edits()
    val onlyCheck = true
    if (onlyCheck) {
        println("checkProductionDraft: ${checkProductionDraft(edits)}")
    } else if (updateProductionDraft(edits)) {
        println("update successful")
        println("checkProductionDraft: ${checkProductionDraft(edits)}")
    } else {
        println("update failed")
    }
}

private fun googleCredentials() =
    ServiceAccountCredentials.fromStream(FileInputStream(serviceAccountKey))
        .createScoped(listOf(AndroidPublisherScopes.ANDROIDPUBLISHER))

private fun androidPublisherService(credentials: GoogleCredentials) =
    AndroidPublisher.Builder(
        GoogleNetHttpTransport.newTrustedTransport(),
        GsonFactory.getDefaultInstance(),
        HttpCredentialsAdapter(credentials)
    ).setApplicationName(packageName).build()

private fun editId(edits: AndroidPublisher.Edits) = edits.insert(packageName, null).execute().id

private fun updateProductionDraft(edits: AndroidPublisher.Edits): Boolean {
    val editId = editId(edits)
    val trackList = tracks(edits, editId)
    val track = track(trackList) ?: return false.also {
        println("update: no track available")
        log(trackList)
    }
    val releases = trackRelease(track) ?: return false.also {
        println("update: no release draft track available")
        log(track)
    }
    releases.inAppUpdatePriority = 3
    edits.tracks().update(packageName, editId, track.track, track).execute()
    edits.commit(packageName, editId).execute()
    return true
}

private fun tracks(
    edits: AndroidPublisher.Edits, editId: String
): List<Track> = edits.tracks().list(packageName, editId).execute().tracks

private fun checkProductionDraft(edits: AndroidPublisher.Edits): Boolean {
    val editId = editId(edits)
    val trackList = tracks(edits, editId)
    val track = track(trackList) ?: return false.also {
        println("check: no track available")
        log(trackList)
    }
    val releases = trackRelease(track) ?: return false.also {
        println("check: no release draft track available")
        log(track)
    }
    log(releases, "check release")
    return true
}

private fun track(trackList: List<Track>): Track? = trackList.firstOrNull { it.track == "production" }

private fun trackRelease(track: Track): TrackRelease? = track.releases?.firstOrNull { it.status == "draft" }

private fun log(src: Any, tag: String? = null) {
    val start = "$tag:\n".takeUnless { tag.isNullOrBlank() }.orEmpty()
    println("$start${gson.toJson(src)}\n\n")
}
