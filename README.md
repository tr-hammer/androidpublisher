# androidpublisher

## Prerequisites

#### Put `truckers_report_service_account.json` into project dir

## Documentation

#### [Android In-app updates](https://developer.android.com/guide/playcore/in-app-updates)

[Support in-app updates (Kotlin or Java) ](https://developer.android.com/guide/playcore/in-app-updates/kotlin-java)

#### [Method: edits.tracks.update](https://developers.google.com/android-publisher/api-ref/rest/v3/edits.tracks/update)

[Google sample](https://github.com/googlesamples/android-play-publisher-api/tree/master/v3/java)

## External libraries

#### [google-api-services-androidpublisher](https://github.com/googleapis/google-api-java-client-services/tree/main/clients/google-api-services-androidpublisher/v3)

#### [google-auth-library-oauth2-http](https://github.com/googleapis/google-auth-library-java)
